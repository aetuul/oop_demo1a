public class Esiintyja {
    private String erityisala;
    private String nimi;
    private String id;
    private double palkkio;

    public Esiintyja(String nimi, String id, String erityisala, double palkkio) {
        this.nimi = nimi;
        this.erityisala = erityisala;
        this.palkkio = 0;
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public String getErityisala() {
        return this.erityisala;
    }

    public String getNimi() {
        return this.nimi;
    }

    public double getPalkkio() {
        return this.palkkio;
    }

    public void setPalkkio(double palkkio) {
        this.palkkio = palkkio;
    }
}