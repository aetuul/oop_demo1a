class Tilaisuus {
    private String tyyppi;
    private boolean varattu;
    private Esiintyja esiintyja;
    private String id;
    private double palkkio;

    public Tilaisuus(String id, String tyyppi, Esiintyja esiintyja, double palkkio, boolean varattu) {
        this.tyyppi = tyyppi;
        this.esiintyja = esiintyja;
        this.palkkio = palkkio;
        this.varattu = false;
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public String getTyyppi() {
        return this.tyyppi;
    }

    public boolean getVarattu() {
        return this.varattu;
    }

    public Esiintyja getEsiintyja() {
        return this.esiintyja;
    }

    public double getPalkkio() {
        return this.palkkio;
    }

    public void varaa(Esiintyja esiintyja, double palkkio) {
        if (this.varattu) {
            return;
        }
        this.varattu = true;
        this.esiintyja = esiintyja;
        this.palkkio = palkkio;
    }

    public void maksaPalkkio() {
        if (!this.varattu) {
            return;
        }
        this.esiintyja.setPalkkio(this.palkkio);
    }
}
