public class Asiakas {
    private String id;
    private String nimi;

    public Asiakas(String id, String nimi) {
        this.id = id;
        this.nimi = nimi;
    }

    public String getId() {
        return this.id;
    }

    public String getNimi() {
        return this.nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }
}